===========
Slide Tools
===========

This is a set of LaTeX macros and sundry other stuff intended to aid in
the production of course materials and related slides, based on
latex-beamer.


Slidetool Features
------------------

* Use ``\begin{onlyenv}<article>`` for article-only material within
  slides, or write outside of frame environments. Within in paragraphs,
  use \only<article>{}.
* ``\slidefill`` – a fill cancelling beamer's centering on slides
* ``\smallverbatim``, ``\tinyverbatim`` – use a small (or criminally
  small, in the second case) tt font in the verbatim environment.
* ``\attachthis{path}{anchor text}`` – attach a file; against stock
  attachfile, you can define a macro ``\attachdir`` (defaulting to
  ``.``) to make attachfile look for the files there; that's nice when
  you assemble your notes from several projects.
* ``\simplefigure{hratio}{path} – put the image at path at width
  hratio ⋅ \textwidth at the current position and center it.
* ``\userinput``, ``\screenprompt``, ``\menubutton`` – mark up for stuff users
  will type, things written on the screen or menu items or buttons.
* ``\KW`` – “keyword”, something that should become an index term or so.

Exercises
'''''''''

Have exercises in an Exercise environment:

\begin{Exercise}
\begin{Question}
\end{Question}
\begin{Answer}
\end{Answer}
\end{Exercise}

The exercises will produce no output in the lecture slides.

When making notes, the problems will be included in-line.  The whole
exercise environments will be extracted to a file exercises-extract.tex
To have the answers in, say, an appendix, say \inputanswers.  You cannot
defined any more problems once you did that.

To make slides from the exercises, say make exercises.pdf.

Landing Page
''''''''''''

The built products of slidetools are intended to live in some sort of
landing page with links to the various PDFs.  To enable that, define an
``INSTALL_TARGET`` in your makefile, giving something suitable as an
rsync target; make sure you have a trailing slash.

The content of the landing page is defined as an xhtml div element in a
file ``landingpage.html``.  Use ``class="abstract"`` in a container
element for an abstract, and you probably want something like::

  <p class="reslinks">First things first:
    <a href="notes.pdf">Lecture Notes</a>
    | <a href="slides.pdf">Slides</a>
    | <a href="exercises.pdf">Exercises as slides</a>.</p>

pretty far up.  The h1 of that file is used as the title of the landing
page.  When you have written this, run ``make install``.


Starting a Document
-------------------

To start a document, create a directory and create a LaTeX-Beamer file
in there.  By default, that file's name should be course.tex, but you
can override that.

Then, say::

  git clone https://codeberg.org/hendhd/slidetools
  cp slidetools/gitignore-template .gitignore
  cp slidetools/Makefile.template Makefile

In Makefile.template, change the name of ``MAIN_DOC`` if necessary, and add
any other dependencies of your documents in ``SOURCES``.


Makefile Targets
----------------

* ``slides.pdf`` (default) – produce a PDF for slides
* ``notes.pdf`` – produce a PDF with lecture notes
* ``exercises.pdf`` – produce slides with the problems.
* ``clean``
* ``update`` – syncs your slide tools with what is currently in the
  version control.  Set the environment variables ``SLIDETOOLS`` and
  ``SLIDETOOLS_BRANCH`` to pull from non-default locations when you
  develop the slidetools themselves.
