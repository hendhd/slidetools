"""
A quick hack to support the generation of HTML landing pages.
"""

import io
import re
import sys
from xml.etree import ElementTree


################ Micro templating start
def escapePCDATA(val):
	if val is None:
		return ""

	return str(val
		).replace("&", "&amp;"
		).replace('<', '&lt;'
		).replace('>', '&gt;'
		).replace("\0", "&x00;")


def escapeAttrVal(val):
	"""returns val with escapes for double-quoted attribute values.
	"""
	if val is None:
		return '""'
	return escapePCDATA(val).replace('"', '&quot;')


class Template:
	"""a *very* basic and ad-hoc template engine.

	It works on HTML strings, with the following constructs expanded:

	* $[key] -- value for key, escaped for double-quoted att values
	* $(key) -- value for key, escaped for PCDATA
	* $|func| -- replace with the value of func(vars)
	* $!raw! -- value for key, non-escaped (other template ops are expanded)
	* $$ -- a $ char.

	Use either unicode strings or plain ASCII
	"""
	def __init__(self, source):
		self.source = str(source)

	def render(self, vars):
		"""returns a string with the template filled using vars.

		vars is a dictionary mapping keys to unicode-able objects.

		You'll get back a unicode string that you must encode before
		spitting it out to the web.
		"""
		return re.sub(r"\$\$", "$",
			re.sub(r"\$\|([a-zA-Z0-9_]+)\|",
				lambda mat: globals()[mat.group(1)](vars),
			re.sub(r"\$\(([a-zA-Z0-9_]+)\)",
				lambda mat: escapePCDATA(vars.get(mat.group(1), "")),
			re.sub(r"\$\[([a-zA-Z0-9_]+)\]",
				lambda mat: escapeAttrVal(vars.get(mat.group(1), "")),
			re.sub(r"\$!([a-zA-ZÄÖÜäöüß0-9_]+)!",
				lambda mat: str(vars.get(mat.group(1), "")),
			self.source)))))


def main():
	with open("slidetools/index-template.html", encoding="utf-8") as f:
		tpl = Template(f.read())
	with open("landingpage.html", encoding="utf-8") as f:
		content = ElementTree.parse(f)

	vars = {}
	vars["title"] = content.find("h1").text
	buf = io.BytesIO()
	content.write(buf, encoding="utf-8")
	vars["content"] = buf.getvalue().decode("utf-8")

	with open(sys.argv[1], "w", encoding="utf-8") as f:
		f.write(tpl.render(vars))


if __name__=="__main__":
	main()
