%
% Slide tools for lecture slides and handouts.
% Slide tools is extending the LaTeX beamer class.
%
%
%
% LICENSE
%
% To the extend possible under law, this code is not copyrighted but
% dedicated to the public domain and anybody caught singin' it without our
% permission, will be mighty good friends of ourn, cause we don't give a
% dern. Publish it. Write it. Copy it. Extend it. Sell it. Buy it. Sing it.
% Swing to it. Yodel it. We wrote it, that's all we wanted to do.
%
% You should have received a copy of the CC0 legalcode along with this
% work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{slidetools}[2023/11/21 Standard LaTeX Class]

% To satisfy the need of slides and a longer version, we use \myMode
% which is set in Makefile through something like
% pdflatex -jobname=long '\def\myMode{a}\input talk.tex'
% Where n is for article mode and p for presentation.
%
% TODO: for HTML it might be necessary to add another condition

\makeatletter

\if\myMode p
  \LoadClass[ignorenonframetext]{beamer}
  \usetheme{metropolis}

\else
  \if\myMode n
    \LoadClass[11pt]{article}
  	\RequirePackage{geometry}
  	\geometry{a4paper,hmargin={2cm,6cm},marginparwidth=5cm,vmargin={2cm,3cm}}
    \RequirePackage{mathpazo}
  	\RequirePackage[hyperref]{beamerarticle}
  	\RequirePackage{fullpage}
		\RequirePackage{changepage}
  \else
  	\errmessage{Unknown mode \myMode}
  \fi
\fi

\RequirePackage[color={0.318 0 0.318}]{attachfile2}
\RequirePackage{verbatim}
\RequirePackage{xkeyval}
\RequirePackage{tikz}
\RequirePackage{pifont}
\RequirePackage{xcolor}
\RequirePackage{tcolorbox}
\RequirePackage{upquote}

\definecolor{inputcolor}{rgb}{0.0,0.7,0.1}
\definecolor{promptcolor}{rgb}{0.0,0.1,0.7}
\definecolor{linkcolor}{rgb}{0.318,0,0.9}
\hypersetup{colorlinks,
  breaklinks,
  linkcolor=linkcolor,
  anchorcolor=linkcolor,
  citecolor=linkcolor,
  urlcolor=linkcolor}
\urlstyle{same}

\newcommand{\userinput}[1]{{\color{inputcolor}\bfseries #1}}
\newcommand{\screenprompt}[1]{{\color{promptcolor}\itshape #1}}
\definecolor{menubgcolor}{RGB}{210,210,240}
\definecolor{menufcolor}{RGB}{140,140,170}
\NewTCBox{\menubutton}{s}{nobeforeafter,tcbox raise base,
	top=0pt,bottom=0pt,left=0mm,right=0mm,
	leftrule=0pt,rightrule=0pt,toprule=0.3mm,bottomrule=0.3mm,boxsep=0.5mm,
	colback=menubgcolor, colframe=menufcolor}
\newcommand{\menu}[1]{\tcbox[colback=menubgcolor]{\hbox{\strut #1}}}
\newcommand{\xmlel}[1]{{\ttfamily\itshape #1}}
\newcommand{\KW}[1]{{\itshape #1}}

\def\verbatim@font{\fontsize{9pt}{11pt}\ttfamily
    \hyphenchar\font\m@ne
    \@noligs\selectfont}
\def\smallverbatim{%
	\def\verbatim@font{\fontsize{8pt}{10pt}\ttfamily
    \hyphenchar\font\m@ne
    \@noligs\selectfont}}
\def\tinyverbatim{%
	\def\verbatim@font{\fontsize{6pt}{7pt}\ttfamily
    \hyphenchar\font\m@ne
    \@noligs\selectfont}}

\def\attachdir{.}
\def\attachthis#1#2{\attachfile{\attachdir/#1}{#2}}

% attachfile's pushpin is far too large for us.  Until I have time
% to devise something more appropriate, use a Psi instead.
\def\atfi@acroPushPin{{\color{linkcolor}$\Psi$}}

\def\simplefigure#1#2{
  \hbox to\textwidth{\hfil
  	\includegraphics[width=#1\textwidth]{#2}\hfil}}

\newcounter{Exercise}

\mode
<article>
  \def\slidefill{\bigbreak}
  \def\slidegobble{\relax}
  \parindent=0pt
  \parskip=3pt plus 4pt

	% we want to extract the answers for the appendix.  For convenience,
	% we define the Answer environment globally to do that (and discard
	% whatever is in it).
	\newenvironment{Answer}{\begingroup\setbox0\vbox\bgroup}{\egroup\endgroup}
	\newenvironment{Question}{\par\begingroup\raggedright
		\begin{adjustwidth}{1cm}{}%
	  \textbf{Exercise \arabic{Exercise}}\par}{\par\end{adjustwidth}\endgroup}

  \usepackage[active,
    generate=exercises-extract,
	  extract-env={Question,Answer},
  	copydocumentclass=false,
  	handles=false,
  	header=false]{extract}

	% we want the question inline in a standalone presentation.
	% let's see how that works out
	\newenvironment{Exercise}{%
		\refstepcounter{Exercise}%
		\immediate\write\XTR@out{\def\noexpand\exno{\arabic{Exercise}}
		}}{}

	\def\inputanswers{\immediate\closeout\XTR@out
 		\renewenvironment{Answer}{\paragraph{Solution for Exercise \exno}}{}
 		\renewenvironment{Question}
  		{\begingroup\setbox0\vbox\bgroup}{\egroup\endgroup}
		\input exercises-extract}

	\def\link#1#2{#2\footnote{\url{#1}}}
\mode
<presentation>
  \def\link#1#2{\href{#1}{#2}}
  \def\slidefill{\vfill}
  \def\slidegobble{\vfilneg}
	% We don't need to do anything about exercises, since they're
	% outside of frames and we are building with ignorenoframetext
\mode
<all>
\makeatother

% experimental macro to suggest tasks yielding a "Schein".  This
% will probably go away again
\def\scheinproblem#1{\relax}
