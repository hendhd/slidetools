
SLIDESOPS = -jobname="slides" -pretex="\def\myMode{p}" -usepretex
NOTESOPS = -jobname="notes" -pretex="\def\myMode{n}" -usepretex

export TEXINPUTS=.:slidetools:

TEX = latexmk -lualatex -pdf

slides.pdf: $(SOURCES)
	$(TEX) $(SLIDESOPS) $(MAIN_DOC)

notes.pdf:  $(SOURCES)
	$(TEX) $(NOTESOPS) $(MAIN_DOC)

exercises.pdf: notes.pdf slidetools/exercises.tex
	$(TEX) -pretex="\def\myMode{p}" -usepretex slidetools/exercises.tex

clean:
#	latexmk -C $(MAIN_DOC) slidetools.cls
# darn. This is needed for now, until a decision is made wether to use a
# TeX-Class or several files instead. The unelegance of the following
# lines actually is a strong point toward not using slidetools.cls
	rm -f *.pdf *.aux *.fdb_latexmk *.fls *.log *.nav *.out *.snm \
		*.atfi *toc *.xtr exercises.tex

SLIDETOOLS_BRANCH ?= main

update:
	cd slidetools && git checkout $(SLIDETOOLS_BRANCH) && git pull

install: notes.pdf slides.pdf exercises.pdf landingpage.html slidetools/index-template.html
	mkdir -p webpage
	python slidetools/make-indexhtml.py webpage/index.html
	cp -a notes.pdf slides.pdf exercises.pdf webpage
	rsync -av webpage/* ${INSTALL_TARGET}
